<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>calendar</title>
    </head>
    <style>
    </style>
    <body>
<?php
//年、月
$year = date('Y');
$month = date('n');
//月末を取得
$timestamp = mktime(0,0,0,$month,1,$year);
$last_day = date('t', $timestamp);
$frist_day_week = date('w', $timestamp);
$last_day_week = date('w', mktime(0,0,0,$month,$last_day,$year));
$calendar = array();
//曜日文字
$weektxt = array('日','月','火','水','木','金','土');

?>
<table>
    <thead>
        <tr>
            <th>日</th>
            <th>月</th>
            <th>火</th>
            <th>水</th>
            <th>木</th>
            <th>金</th>
            <th>土</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php
              for($i=0; $i<$frist_day_week; $i++){
                    echo '<td></td>';
              }
              
              for($j=1; $j<=$last_day; $j++){
                    echo '<td>' . $j . '</td>';
                  
                    if(($j+$i)%7==0){
                    echo '</tr><tr>';
                  }
              }
              for($k=$last_day_week; $k<6; $k++){
                    echo '<td></td>';
              }
            ?>
        </tr>
    </tbody>
</table>

    </body>
</html>

